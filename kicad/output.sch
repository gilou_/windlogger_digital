EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Windlogger digital"
Date "2020-02-21"
Rev "1.2.1"
Comp "ALEEA"
Comment1 "LONGUET Gilles"
Comment2 "AGPL3"
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1925 4050 0    60   ~ 0
Radio module : rfm69cw
$Comp
L digital-rescue:RFM69CW-digital Mod3
U 1 1 5805B811
P 2600 4950
F 0 "Mod3" H 2600 4550 60  0000 C CNN
F 1 "RFM69CW" H 2550 5500 60  0000 C CNN
F 2 "windlog:RFM69CW" H 2600 4950 60  0001 C CNN
F 3 "" H 2600 4950 60  0000 C CNN
	1    2600 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 4550 1950 4550
Wire Wire Line
	1500 4650 1950 4650
Wire Wire Line
	1850 5150 1950 5150
Text Label 1500 4550 0    60   ~ 0
MISO_3V3
Text Label 1500 4650 0    60   ~ 0
RF_IRQ_3V3
Wire Wire Line
	3200 4550 3425 4550
Wire Wire Line
	4150 4650 3200 4650
Wire Wire Line
	4150 4750 3200 4750
Text Label 4150 4550 2    60   ~ 0
RF_CS_3V3
Text Label 4150 4650 2    60   ~ 0
SCLK_3V3
Text Label 4150 4750 2    60   ~ 0
MOSI_3V3
NoConn ~ 1950 4750
NoConn ~ 1950 4850
NoConn ~ 1950 4950
NoConn ~ 1950 5050
NoConn ~ 3200 4850
Wire Wire Line
	3200 4950 3275 4950
$Comp
L digital-rescue:GND-power-digital-rescue #PWR040
U 1 1 5805C11A
P 2600 5550
F 0 "#PWR040" H 2600 5300 50  0001 C CNN
F 1 "GND" H 2600 5400 50  0000 C CNN
F 2 "" H 2600 5550 50  0000 C CNN
F 3 "" H 2600 5550 50  0000 C CNN
	1    2600 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3325 5050 3200 5050
Text Notes 5750 1325 0    60   ~ 0
GPRS module
NoConn ~ 7100 1950
NoConn ~ 7100 2050
NoConn ~ 7100 2150
NoConn ~ 7100 2250
NoConn ~ 7100 2350
NoConn ~ 7100 2450
$Comp
L digital-rescue:GND-power-digital-rescue #PWR050
U 1 1 587270D1
P 5625 2450
F 0 "#PWR050" H 5625 2200 50  0001 C CNN
F 1 "GND" H 5625 2300 50  0000 C CNN
F 2 "" H 5625 2450 50  0000 C CNN
F 3 "" H 5625 2450 50  0000 C CNN
	1    5625 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5625 2450 5625 2350
Wire Wire Line
	5625 2350 5700 2350
Wire Wire Line
	5700 2250 5000 2250
Wire Wire Line
	5700 2150 5000 2150
Wire Wire Line
	5700 2050 5000 2050
$Comp
L digital-rescue:SIM800L-digital Mod5
U 1 1 5874B600
P 6400 2100
F 0 "Mod5" H 5950 1600 60  0000 C CNN
F 1 "SIM800L" H 6100 2600 60  0000 C CNN
F 2 "windlog:GPRS-SIM80L-smaller" H 6400 2100 60  0001 C CNN
F 3 "" H 6400 2100 60  0000 C CNN
	1    6400 2100
	1    0    0    -1  
$EndComp
Text Notes 4900 2400 0    60   ~ 0
3.3V tolerance
$Comp
L digital-rescue:R-Device-digital-rescue R6
U 1 1 58756547
P 3425 4250
F 0 "R6" V 3505 4250 50  0000 C CNN
F 1 "100k" V 3425 4250 50  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3355 4250 50  0001 C CNN
F 3 "" H 3425 4250 50  0000 C CNN
	1    3425 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3425 4400 3425 4550
Connection ~ 3425 4550
Wire Wire Line
	3425 4100 3425 4050
Wire Wire Line
	3425 4050 3325 4050
Connection ~ 3325 4050
Text Notes 3900 4250 0    60   ~ 0
SMA antenna \nconnector 433 MHz
$Comp
L digital-rescue:GND-power-digital-rescue #PWR044
U 1 1 58756BE4
P 3800 5425
F 0 "#PWR044" H 3800 5175 50  0001 C CNN
F 1 "GND" H 3800 5275 50  0000 C CNN
F 2 "" H 3800 5425 50  0000 C CNN
F 3 "" H 3800 5425 50  0000 C CNN
	1    3800 5425
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:R-Device-digital-rescue R7
U 1 1 58753060
P 7725 1400
F 0 "R7" V 7805 1400 50  0000 C CNN
F 1 "270" V 7725 1400 50  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7655 1400 50  0001 C CNN
F 3 "" H 7725 1400 50  0000 C CNN
	1    7725 1400
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR053
U 1 1 58753262
P 7725 2100
F 0 "#PWR053" H 7725 1850 50  0001 C CNN
F 1 "GND" H 7725 1950 50  0000 C CNN
F 2 "" H 7725 2100 50  0000 C CNN
F 3 "" H 7725 2100 50  0000 C CNN
	1    7725 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7725 800  7725 925 
Wire Wire Line
	7725 1125 7725 1250
Wire Wire Line
	7725 1550 7725 1600
Wire Wire Line
	7725 2000 7725 2100
Wire Wire Line
	7275 1450 7275 1800
Wire Wire Line
	7275 1450 5700 1450
Wire Wire Line
	5700 1450 5700 1850
Text Label 5700 1450 0    60   ~ 0
NETLIGHT
$Comp
L digital-rescue:+BATT-power-digital-rescue #PWR049
U 1 1 58755363
P 5450 1250
F 0 "#PWR049" H 5450 1100 50  0001 C CNN
F 1 "+BATT" H 5450 1390 50  0000 C CNN
F 2 "" H 5450 1250 50  0000 C CNN
F 3 "" H 5450 1250 50  0000 C CNN
	1    5450 1250
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR051
U 1 1 5875CF72
P 7575 4700
F 0 "#PWR051" H 7575 4550 50  0001 C CNN
F 1 "+5V" H 7575 4840 50  0000 C CNN
F 2 "" H 7575 4700 50  0000 C CNN
F 3 "" H 7575 4700 50  0000 C CNN
	1    7575 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7675 4700 7575 4700
$Comp
L digital-rescue:GND-power-digital-rescue #PWR054
U 1 1 5875D080
P 7425 4900
F 0 "#PWR054" H 7425 4650 50  0001 C CNN
F 1 "GND" H 7425 4750 50  0000 C CNN
F 2 "" H 7425 4900 50  0000 C CNN
F 3 "" H 7425 4900 50  0000 C CNN
	1    7425 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 5400 7675 5400
Wire Wire Line
	6800 5300 7675 5300
Wire Wire Line
	9650 5400 8675 5400
Text Label 9650 5400 2    60   ~ 0
output_uart_reset
$Comp
L digital-rescue:R-Device-digital-rescue R5
U 1 1 58763FEA
P 1900 7000
F 0 "R5" V 1800 7000 50  0000 C CNN
F 1 "470k" V 1900 7000 50  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1830 7000 50  0001 C CNN
F 3 "" H 1900 7000 50  0000 C CNN
	1    1900 7000
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR039
U 1 1 5876412D
P 1900 5950
F 0 "#PWR039" H 1900 5800 50  0001 C CNN
F 1 "+5V" H 1900 6090 50  0000 C CNN
F 2 "" H 1900 5950 50  0000 C CNN
F 3 "" H 1900 5950 50  0000 C CNN
	1    1900 5950
	1    0    0    -1  
$EndComp
Text Label 2275 7375 0    60   ~ 0
MISO_3V3
Text Label 2275 7175 0    60   ~ 0
MOSI_3V3
Text Label 2275 7275 0    60   ~ 0
SCLK_3V3
Text Label 2275 7075 0    60   ~ 0
SD_CS_3V3
$Comp
L digital-rescue:GND-power-digital-rescue #PWR041
U 1 1 5876514C
P 2725 7550
F 0 "#PWR041" H 2725 7300 50  0001 C CNN
F 1 "GND" H 2725 7400 50  0000 C CNN
F 2 "" H 2725 7550 50  0000 C CNN
F 3 "" H 2725 7550 50  0000 C CNN
	1    2725 7550
	1    0    0    -1  
$EndComp
Connection ~ 2725 7475
Wire Wire Line
	5700 1950 5450 1950
Wire Wire Line
	5450 1950 5450 1250
Text HLabel 1325 3025 0    60   Input ~ 0
output_RX
Text HLabel 10625 2600 2    60   Output ~ 0
output_TX
Text HLabel 9650 5400 2    60   Input ~ 0
output_uart_reset
Text HLabel 1300 975  0    60   Input ~ 0
SD_CS
Text HLabel 10550 1500 2    60   Output ~ 0
MISO
Text HLabel 1300 2150 0    60   Input ~ 0
SCLK
Text HLabel 1300 1750 0    60   Input ~ 0
MOSI
Text HLabel 10550 1200 2    60   Output ~ 0
RF_IRQ
Text HLabel 1300 1325 0    60   Input ~ 0
RF_CS
$Comp
L digital-rescue:CONN-SMA-5-1814400-1-digital J1
U 1 1 58858181
P 3900 5150
AR Path="/58858181" Ref="J1"  Part="1" 
AR Path="/5804AD31/58858181" Ref="J1"  Part="1" 
F 0 "J1" H 3900 5350 50  0000 C CNN
F 1 "CONN-SMA-5-1814400-1" H 3900 5250 50  0000 C CNN
F 2 "windlog:conn-SMA_TE-5-1814400-1" H 3900 5150 50  0001 C CNN
F 3 "" H 3900 5150 50  0000 C CNN
	1    3900 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3200 5150 3800 5150
Wire Wire Line
	3800 5250 3800 5300
Wire Wire Line
	3800 5250 3850 5250
Wire Wire Line
	3850 5300 3800 5300
Connection ~ 3800 5300
Wire Wire Line
	3800 5350 3850 5350
Connection ~ 3800 5350
Wire Wire Line
	3850 5400 3800 5400
Connection ~ 3800 5400
$Comp
L digital-rescue:+3.3V-power-digital-rescue #PWR043
U 1 1 58878826
P 3325 3900
F 0 "#PWR043" H 3325 3750 50  0001 C CNN
F 1 "+3.3V" H 3325 4040 50  0000 C CNN
F 2 "" H 3325 3900 50  0000 C CNN
F 3 "" H 3325 3900 50  0000 C CNN
	1    3325 3900
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+3.3V-power-digital-rescue #PWR042
U 1 1 58881CD3
P 3100 6675
F 0 "#PWR042" H 3100 6525 50  0001 C CNN
F 1 "+3.3V" H 3100 6815 50  0000 C CNN
F 2 "" H 3100 6675 50  0000 C CNN
F 3 "" H 3100 6675 50  0000 C CNN
	1    3100 6675
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:LED_Small-Device-digital-rescue D7
U 1 1 5888B31B
P 7725 1025
F 0 "D7" H 7675 1150 50  0000 L CNN
F 1 "Led_Small" H 7550 925 50  0000 L CNN
F 2 "LED_THT:LED_D3.0mm_Horizontal_O1.27mm_Z2.0mm" V 7725 1025 50  0001 C CNN
F 3 "" V 7725 1025 50  0000 C CNN
	1    7725 1025
	0    -1   -1   0   
$EndComp
Text Label 5000 2250 0    60   ~ 0
output_TX_3V3
Text Label 5000 2050 0    60   ~ 0
output_uart_reset
Wire Wire Line
	1300 1325 1775 1325
Wire Wire Line
	2375 1325 3350 1325
Text Label 3350 1325 2    60   ~ 0
RF_CS_3V3
Wire Wire Line
	1525 975  1300 975 
Wire Wire Line
	2125 975  3050 975 
Wire Wire Line
	1525 1750 1300 1750
Wire Wire Line
	1775 2150 1300 2150
Wire Wire Line
	3300 2150 2375 2150
Text Label 9125 1500 0    60   ~ 0
MISO_3V3
Text Label 3300 2150 2    60   ~ 0
SCLK_3V3
Wire Wire Line
	1325 3025 1775 3025
Wire Wire Line
	10475 2600 10625 2600
Wire Wire Line
	3300 3025 2375 3025
Text Label 3300 3025 2    60   ~ 0
output_RX_3V3
Text Label 6800 5300 0    60   ~ 0
output_RX_3V3
Text Label 5000 2150 0    60   ~ 0
output_RX_3V3
$Comp
L digital-rescue:+3V3-power-digital-rescue #PWR045
U 1 1 58B49E77
P 3850 725
F 0 "#PWR045" H 3850 575 50  0001 C CNN
F 1 "+3V3" H 3850 865 50  0000 C CNN
F 2 "" H 3850 725 50  0000 C CNN
F 3 "" H 3850 725 50  0000 C CNN
	1    3850 725 
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR046
U 1 1 58B4A371
P 3850 1900
F 0 "#PWR046" H 3850 1650 50  0001 C CNN
F 1 "GND" H 3850 1750 50  0000 C CNN
F 2 "" H 3850 1900 50  0000 C CNN
F 3 "" H 3850 1900 50  0000 C CNN
	1    3850 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1850 3850 1900
Wire Wire Line
	9125 1500 10550 1500
Wire Wire Line
	9125 1200 10550 1200
Wire Wire Line
	8875 2600 9625 2600
Text Label 9125 1200 0    60   ~ 0
RF_IRQ_3V3
Text Label 8875 2600 0    60   ~ 0
output_TX_3V3
Text Label 6800 5400 0    60   ~ 0
output_TX_3V3
Text Label 3075 1750 2    60   ~ 0
MOSI_3V3
Text Notes 1250 625  0    60   ~ 0
MCU to outputs levels shifter
Wire Notes Line
	5225 7775 5225 3275
Wire Notes Line
	5225 3275 500  3275
Wire Notes Line
	11200 3400 8375 3400
Wire Notes Line
	8375 3400 8375 475 
Text Notes 6700 3550 0    60   ~ 0
UART Modules\n
Text Notes 2500 3575 0    60   ~ 0
SPI modules
$Comp
L digital-rescue:C-Device-digital-rescue C20
U 1 1 58B9EA5B
P 4425 1375
F 0 "C20" H 4450 1475 50  0000 L CNN
F 1 "100n" H 4450 1275 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 4463 1225 50  0001 C CNN
F 3 "" H 4425 1375 50  0000 C CNN
	1    4425 1375
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+3V3-power-digital-rescue #PWR047
U 1 1 58B9EAEE
P 4425 1175
F 0 "#PWR047" H 4425 1025 50  0001 C CNN
F 1 "+3V3" H 4425 1315 50  0000 C CNN
F 2 "" H 4425 1175 50  0000 C CNN
F 3 "" H 4425 1175 50  0000 C CNN
	1    4425 1175
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR048
U 1 1 58B9EB38
P 4425 1575
F 0 "#PWR048" H 4425 1325 50  0001 C CNN
F 1 "GND" H 4425 1425 50  0000 C CNN
F 2 "" H 4425 1575 50  0000 C CNN
F 3 "" H 4425 1575 50  0000 C CNN
	1    4425 1575
	1    0    0    -1  
$EndComp
Wire Wire Line
	4425 1575 4425 1525
Wire Wire Line
	4425 1225 4425 1175
$Comp
L digital-rescue:C-Device-digital-rescue C19
U 1 1 58BA1E92
P 950 4950
F 0 "C19" H 975 5050 50  0000 L CNN
F 1 "100n" H 975 4850 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 988 4800 50  0001 C CNN
F 3 "" H 950 4950 50  0000 C CNN
	1    950  4950
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+3V3-power-digital-rescue #PWR036
U 1 1 58BA1E98
P 950 4750
F 0 "#PWR036" H 950 4600 50  0001 C CNN
F 1 "+3V3" H 950 4890 50  0000 C CNN
F 2 "" H 950 4750 50  0000 C CNN
F 3 "" H 950 4750 50  0000 C CNN
	1    950  4750
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR037
U 1 1 58BA1E9E
P 950 5150
F 0 "#PWR037" H 950 4900 50  0001 C CNN
F 1 "GND" H 950 5000 50  0000 C CNN
F 2 "" H 950 5150 50  0000 C CNN
F 3 "" H 950 5150 50  0000 C CNN
	1    950  5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  5150 950  5100
Wire Wire Line
	950  4800 950  4750
$Comp
L digital-rescue:+BATT-power-digital-rescue #PWR052
U 1 1 58BA41F1
P 7725 800
F 0 "#PWR052" H 7725 650 50  0001 C CNN
F 1 "+BATT" H 7725 940 50  0000 C CNN
F 2 "" H 7725 800 50  0000 C CNN
F 3 "" H 7725 800 50  0000 C CNN
	1    7725 800 
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:R-Device-digital-rescue R8
U 1 1 58BE8C6E
P 9625 2250
F 0 "R8" V 9705 2250 50  0000 C CNN
F 1 "100k" V 9625 2250 50  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9555 2250 50  0001 C CNN
F 3 "" H 9625 2250 50  0000 C CNN
	1    9625 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9625 2400 9625 2600
Connection ~ 9625 2600
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR055
U 1 1 58BE9891
P 9625 2025
F 0 "#PWR055" H 9625 1875 50  0001 C CNN
F 1 "+5V" H 9625 2165 50  0000 C CNN
F 2 "" H 9625 2025 50  0000 C CNN
F 3 "" H 9625 2025 50  0000 C CNN
	1    9625 2025
	1    0    0    -1  
$EndComp
Wire Wire Line
	9625 2025 9625 2100
Wire Wire Line
	3425 4550 4150 4550
Wire Wire Line
	3325 4050 3325 5050
Wire Wire Line
	2725 7475 2725 7550
Wire Wire Line
	3800 5300 3800 5350
Wire Wire Line
	3800 5350 3800 5400
Wire Wire Line
	3800 5400 3800 5425
Wire Wire Line
	9625 2600 9875 2600
Text Notes 3575 7100 0    60   ~ 0
broche pour module carte SD \n
$Comp
L digital-rescue:Conn_01x06_Female-Connector-digital-rescue Mod4
U 1 1 5B93A0EE
P 3450 7175
F 0 "Mod4" H 3477 7151 50  0000 L CNN
F 1 "SD_CARD" H 3477 7060 50  0000 L CNN
F 2 "windlog:SD_PinHeader_1x06_P2.54mm_Vertical" H 3450 7175 50  0001 C CNN
F 3 "~" H 3450 7175 50  0001 C CNN
	1    3450 7175
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 6975 3250 6975
Text HLabel 2000 6700 2    60   Input ~ 0
BP_save_SD
Wire Wire Line
	2725 7475 1900 7475
Wire Wire Line
	1900 7150 1900 7475
Text Label 3050 975  2    60   ~ 0
SD_CS_3V3
Wire Wire Line
	2275 7275 3250 7275
Wire Wire Line
	2275 7375 3250 7375
Wire Wire Line
	2275 7175 3250 7175
Wire Wire Line
	2275 7075 3250 7075
Wire Wire Line
	3100 6675 3100 6975
Wire Wire Line
	2725 7475 3250 7475
Wire Wire Line
	1850 5500 2600 5500
Wire Wire Line
	1850 5150 1850 5500
Wire Wire Line
	3275 4950 3275 5500
Wire Wire Line
	2600 5550 2600 5500
Connection ~ 2600 5500
Wire Wire Line
	2600 5500 3275 5500
Wire Wire Line
	3325 3900 3325 4050
Wire Wire Line
	7425 1800 7275 1800
Wire Wire Line
	2125 1750 3075 1750
Wire Wire Line
	3850 850  3850 725 
Wire Notes Line
	4800 3275 4800 500 
Connection ~ 1900 6700
Wire Wire Line
	1900 6700 2000 6700
Wire Wire Line
	1900 6525 1900 6700
Wire Wire Line
	1900 5950 1900 6125
$Comp
L digital-rescue:SW_Push-Switch-digital-rescue SW2
U 1 1 5BB485DA
P 1900 6325
F 0 "SW2" V 1854 6473 50  0000 L CNN
F 1 "PB_SD_CARD" V 1945 6473 50  0000 L CNN
F 2 "Button_Switch_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 1900 6525 50  0001 C CNN
F 3 "" H 1900 6525 50  0001 C CNN
	1    1900 6325
	0    1    1    0   
$EndComp
$Comp
L digital-rescue:Q_NMOS_GDS-Device-digital-rescue Q2
U 1 1 5BB54417
P 7625 1800
F 0 "Q2" H 7830 1846 50  0000 L CNN
F 1 "Q_NMOS_GDS" H 7830 1755 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3_Handsoldering" H 7825 1900 50  0001 C CNN
F 3 "~" H 7625 1800 50  0001 C CNN
	1    7625 1800
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U5
U 1 1 5BE89D3C
P 1825 975
F 0 "U5" H 1550 1150 50  0000 C CNN
F 1 "4050" H 2125 1150 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 1825 975 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 1825 975 50  0001 C CNN
	1    1825 975 
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U5
U 2 1 5BE8A2D2
P 2075 1325
F 0 "U5" H 1800 1475 50  0000 C CNN
F 1 "4050" H 2375 1475 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 2075 1325 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 2075 1325 50  0001 C CNN
	2    2075 1325
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U5
U 3 1 5BE8A385
P 1825 1750
F 0 "U5" H 1550 1900 50  0000 C CNN
F 1 "4050" H 2125 1900 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 1825 1750 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 1825 1750 50  0001 C CNN
	3    1825 1750
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U5
U 4 1 5BE8A443
P 2075 2150
F 0 "U5" H 1800 2300 50  0000 C CNN
F 1 "4050" H 2350 2300 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 2075 2150 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 2075 2150 50  0001 C CNN
	4    2075 2150
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U5
U 5 1 5BE8A4B0
P 10175 2600
F 0 "U5" H 9925 2775 50  0000 C CNN
F 1 "4050" H 10450 2775 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 10175 2600 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 10175 2600 50  0001 C CNN
	5    10175 2600
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U5
U 6 1 5BE8A55E
P 2075 3025
F 0 "U5" H 1825 3175 50  0000 C CNN
F 1 "4050" H 2350 3175 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 2075 3025 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 2075 3025 50  0001 C CNN
	6    2075 3025
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4050 U5
U 7 1 5BE8A61D
P 3850 1350
F 0 "U5" H 3625 1900 50  0000 L CNN
F 1 "4050" H 3950 1900 50  0000 L CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 3850 1350 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/intersil/documents/cd40/cd4050bms.pdf" H 3850 1350 50  0001 C CNN
	7    3850 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 6700 1900 6850
Wire Wire Line
	7675 4800 7425 4800
Wire Wire Line
	7425 4800 7425 4900
NoConn ~ 7675 4900
NoConn ~ 7675 5000
NoConn ~ 7675 5100
NoConn ~ 7675 5200
NoConn ~ 8675 4700
$Comp
L wemos_mini:WeMos_mini U6
U 1 1 5E52C78A
P 8175 5050
F 0 "U6" H 8175 5793 60  0000 C CNN
F 1 "WeMos_mini" H 8175 5687 60  0000 C CNN
F 2 "digital:D1_mini_board_topview" H 8175 5581 60  0000 C CNN
F 3 "http://www.wemos.cc/Products/d1_mini.html" H 8175 5581 60  0001 C CNN
	1    8175 5050
	1    0    0    -1  
$EndComp
NoConn ~ 8675 4800
NoConn ~ 8675 4900
NoConn ~ 8675 5000
NoConn ~ 8675 5100
NoConn ~ 8675 5200
NoConn ~ 8675 5300
$EndSCHEMATC
