EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "Windlogger digital"
Date "2020-02-21"
Rev "1.2.1"
Comp "ALEEA"
Comment1 "LONGUET Gilles"
Comment2 "AGPL3"
Comment3 ""
Comment4 ""
$EndDescr
Text Label 10825 1250 2    60   ~ 0
AREF
Text Label 8800 1475 0    60   ~ 0
RESET
$Comp
L digital-rescue:GND-power-digital-rescue #PWR06
U 1 1 58047CE8
P 8650 1950
F 0 "#PWR06" H 8650 1700 50  0001 C CNN
F 1 "GND" H 8650 1800 50  0000 C CNN
F 2 "" H 8650 1950 50  0000 C CNN
F 3 "" H 8650 1950 50  0000 C CNN
	1    8650 1950
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR010
U 1 1 58047D8D
P 11000 1350
F 0 "#PWR010" H 11000 1100 50  0001 C CNN
F 1 "GND" H 11000 1200 50  0000 C CNN
F 2 "" H 11000 1350 50  0000 C CNN
F 3 "" H 11000 1350 50  0000 C CNN
	1    11000 1350
	0    -1   -1   0   
$EndComp
$Comp
L digital-rescue:Conn_01x08-Connector_Generic-digital-rescue P3
U 1 1 58047F00
P 9350 1575
F 0 "P3" H 9425 1975 50  0000 C CNN
F 1 "Power" V 9450 1575 50  0000 C CNN
F 2 "windlog:PinSocket_1x08_P2.54mm_Vertical" H 9350 1575 50  0001 C CNN
F 3 "" H 9350 1575 50  0000 C CNN
	1    9350 1575
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:Conn_01x08-Connector_Generic-digital-rescue P4
U 1 1 58047F99
P 9350 2525
F 0 "P4" H 9450 2925 50  0000 C CNN
F 1 "Analog" V 9450 2525 50  0000 C CNN
F 2 "windlog:PinSocket_1x08_P2.54mm_Vertical" H 9350 2525 50  0001 C CNN
F 3 "" H 9350 2525 50  0000 C CNN
	1    9350 2525
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:Conn_01x08-Connector_Generic-digital-rescue P6
U 1 1 5804800C
P 10000 2400
F 0 "P6" H 10175 2725 50  0000 C CNN
F 1 "PWM2" V 10100 2400 50  0000 C CNN
F 2 "windlog:PinSocket_1x08_P2.54mm_Vertical" H 10000 2400 50  0001 C CNN
F 3 "" H 10000 2400 50  0000 C CNN
	1    10000 2400
	-1   0    0    -1  
$EndComp
$Comp
L digital-rescue:Conn_01x08-Connector_Generic-digital-rescue P7
U 1 1 58048116
P 10000 3350
F 0 "P7" H 9750 3750 50  0000 C CNN
F 1 "Communication" V 10100 3350 50  0000 C CNN
F 2 "windlog:PinSocket_1x08_P2.54mm_Vertical" H 10000 3350 50  0001 C CNN
F 3 "" H 10000 3350 50  0000 C CNN
	1    10000 3350
	-1   0    0    -1  
$EndComp
$Comp
L digital-rescue:Conn_01x10-Connector_Generic-digital-rescue P5
U 1 1 580481BB
P 10000 1450
F 0 "P5" H 10050 1950 50  0000 C CNN
F 1 "PWM1" V 10100 1450 50  0000 C CNN
F 2 "windlog:PinSocket_1x10_P2.54mm_Vertical" H 10000 1450 50  0001 C CNN
F 3 "" H 10000 1450 50  0000 C CNN
	1    10000 1450
	-1   0    0    -1  
$EndComp
$Comp
L digital-rescue:Conn_01x01-Connector_Generic-digital-rescue P9
U 1 1 5804CFD9
P 10725 6075
F 0 "P9" H 11000 6075 50  0000 C CNN
F 1 "hole" H 10850 6075 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 10725 6075 50  0001 C CNN
F 3 "" H 10725 6075 50  0000 C CNN
	1    10725 6075
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:Conn_01x01-Connector_Generic-digital-rescue P10
U 1 1 5804D04C
P 10725 6200
F 0 "P10" H 11000 6200 50  0000 C CNN
F 1 "hole" H 10850 6200 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 10725 6200 50  0001 C CNN
F 3 "" H 10725 6200 50  0000 C CNN
	1    10725 6200
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:Conn_01x01-Connector_Generic-digital-rescue P11
U 1 1 5804D0CA
P 10725 6325
F 0 "P11" H 11000 6325 50  0000 C CNN
F 1 "hole" H 10850 6325 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 10725 6325 50  0001 C CNN
F 3 "" H 10725 6325 50  0000 C CNN
	1    10725 6325
	1    0    0    -1  
$EndComp
NoConn ~ 10525 6075
NoConn ~ 10525 6200
NoConn ~ 10525 6325
$Comp
L digital-rescue:Conn_01x01-Connector_Generic-digital-rescue P8
U 1 1 5804D472
P 10725 5950
F 0 "P8" H 11000 5950 50  0000 C CNN
F 1 "hole" H 10850 5950 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_ISO7380_Pad" H 10725 5950 50  0001 C CNN
F 3 "" H 10725 5950 50  0000 C CNN
	1    10725 5950
	1    0    0    -1  
$EndComp
NoConn ~ 10525 5950
$Comp
L digital-rescue:Conn_01x04-Connector_Generic-digital-rescue P1
U 1 1 5804EF26
P 8825 4525
F 0 "P1" H 8825 4775 50  0000 C CNN
F 1 "POWER_SOURCE1" V 8925 4475 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 8825 4525 50  0001 C CNN
F 3 "" H 8825 4525 50  0000 C CNN
	1    8825 4525
	-1   0    0    -1  
$EndComp
Text Notes 10500 5700 0    60   ~ 0
mecanic holes
Text Notes 8925 825  0    60   ~ 0
Arduino style but with Atmega 1284P
$Comp
L digital-rescue:Conn_02x03_Odd_Even-Connector_Generic-digital-rescue P2
U 1 1 58519427
P 8925 3425
F 0 "P2" H 8925 3625 50  0000 C CNN
F 1 "ISP" H 8925 3225 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x03_P2.54mm_Vertical" H 8925 2225 50  0001 C CNN
F 3 "" H 8925 2225 50  0000 C CNN
	1    8925 3425
	1    0    0    -1  
$EndComp
Text Label 8475 3525 0    60   ~ 0
RESET
$Comp
L digital-rescue:GND-power-digital-rescue #PWR07
U 1 1 5851A232
P 9325 3575
F 0 "#PWR07" H 9325 3325 50  0001 C CNN
F 1 "GND" H 9325 3425 50  0000 C CNN
F 2 "" H 9325 3575 50  0000 C CNN
F 3 "" H 9325 3575 50  0000 C CNN
	1    9325 3575
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR08
U 1 1 5851ACCA
P 9350 3275
F 0 "#PWR08" H 9350 3125 50  0001 C CNN
F 1 "+5V" H 9350 3415 50  0000 C CNN
F 2 "" H 9350 3275 50  0000 C CNN
F 3 "" H 9350 3275 50  0000 C CNN
	1    9350 3275
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR04
U 1 1 586ADB1D
P 8275 1600
F 0 "#PWR04" H 8275 1450 50  0001 C CNN
F 1 "+5V" H 8275 1740 50  0000 C CNN
F 2 "" H 8275 1600 50  0000 C CNN
F 3 "" H 8275 1600 50  0000 C CNN
	1    8275 1600
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+3.3V-power-digital-rescue #PWR05
U 1 1 586AF4A7
P 8650 1250
F 0 "#PWR05" H 8650 1100 50  0001 C CNN
F 1 "+3.3V" H 8650 1390 50  0000 C CNN
F 2 "" H 8650 1250 50  0000 C CNN
F 3 "" H 8650 1250 50  0000 C CNN
	1    8650 1250
	1    0    0    -1  
$EndComp
NoConn ~ 9150 1275
Text Label 8975 5800 0    60   ~ 0
GND
Text Label 8975 6100 0    60   ~ 0
DTR
$Sheet
S 1175 1200 1075 3575
U 587527C3
F0 "microC" 60
F1 "microC.sch" 60
F2 "ACD0" I R 2250 1300 60 
F3 "ACD1" I R 2250 1400 60 
F4 "ACD2" I R 2250 1500 60 
F5 "ACD3" I R 2250 1600 60 
F6 "ACD4" I R 2250 1700 60 
F7 "ACD5" I R 2250 1800 60 
F8 "ACD6" I R 2250 1900 60 
F9 "ACD7" I R 2250 2000 60 
F10 "PB0" I R 2250 2200 60 
F11 "PB1" I R 2250 2300 60 
F12 "PB2" I R 2250 2400 60 
F13 "PB3" O R 2250 2500 60 
F14 "PB4" O R 2250 2600 60 
F15 "PB5" O R 2250 2700 60 
F16 "PB6" I R 2250 2800 60 
F17 "PB7" O R 2250 2900 60 
F18 "PC0" I R 2250 3075 60 
F19 "PC1" I R 2250 3175 60 
F20 "PC2" I R 2250 3275 60 
F21 "PC3" I R 2250 3375 60 
F22 "PC5" I R 2250 3575 60 
F23 "PD0" I R 2250 3950 60 
F24 "PD1" O R 2250 4050 60 
F25 "PD2" I R 2250 4150 60 
F26 "PD3" O R 2250 4250 60 
F27 "PD4" B R 2250 4350 60 
F28 "PD5" B R 2250 4450 60 
F29 "PD6" B R 2250 4550 60 
F30 "PD7" B R 2250 4650 60 
F31 "RESET" I L 1175 2000 60 
F32 "AREF" I L 1175 2600 60 
F33 "PC4" I R 2250 3475 60 
F34 "PC6" I R 2250 3675 60 
F35 "PC7" I R 2250 3775 60 
$EndSheet
Text Label 2900 2700 2    60   ~ 0
D11-MOSI
Text Label 2900 2800 2    60   ~ 0
D12-MISO
Text Label 2875 2900 2    60   ~ 0
D13-SCLK
Text Label 2900 3950 2    60   ~ 0
D0-RX0
Text Label 2900 4050 2    60   ~ 0
D1-TX0
Text Label 2900 4150 2    60   ~ 0
D2-RX1
Text Label 2900 4250 2    60   ~ 0
D3-TX1
Text Label 2900 2300 2    60   ~ 0
D5
Text Label 2900 2200 2    60   ~ 0
D4
Text Label 2900 3075 2    60   ~ 0
D22-SCL
Text Label 2900 3175 2    60   ~ 0
D23-SDA
Text Label 2900 2400 2    60   ~ 0
D6
Text Label 2675 1900 2    60   ~ 0
D15-A6
Text Label 2675 2000 2    60   ~ 0
D14-A7
Text Label 2675 1300 2    60   ~ 0
D21-A0
Text Label 2900 2500 2    60   ~ 0
D7
Text Label 2675 1400 2    60   ~ 0
D20-A1
Text Label 2675 1500 2    60   ~ 0
D19-A2
Text Label 2675 1600 2    60   ~ 0
D18-A3
Text Label 2675 1700 2    60   ~ 0
D17-A4
Text Label 2900 3275 2    60   ~ 0
D24
Text Label 775  2000 0    60   ~ 0
RESET
Text Label 775  2600 0    60   ~ 0
AREF
$Comp
L digital-rescue:C-Device-digital-rescue C1
U 1 1 587726C6
P 675 1700
F 0 "C1" H 700 1800 50  0000 L CNN
F 1 "100n" H 700 1600 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 713 1550 50  0001 C CNN
F 3 "" H 675 1700 50  0000 C CNN
	1    675  1700
	1    0    0    -1  
$EndComp
Text Label 675  1375 0    60   ~ 0
DTR
Text Notes 10025 5475 2    60   ~ 0
Microcontroler Uart to USB bridge
Text Label 2675 1800 2    60   ~ 0
D16-A5
Text Label 2900 2600 2    60   ~ 0
D10
Text Label 2900 3375 2    60   ~ 0
D25
Text Label 2900 4450 2    60   ~ 0
D8
Text Label 2900 4350 2    60   ~ 0
D30
Text Label 2900 4550 2    60   ~ 0
D9
Text Label 2900 4650 2    60   ~ 0
D31
Wire Notes Line
	8150 5225 11200 5225
Wire Wire Line
	675  1375 675  1550
Wire Wire Line
	675  1850 675  2000
Wire Wire Line
	775  2600 1175 2600
Wire Wire Line
	675  2000 1175 2000
Wire Wire Line
	2675 2000 2250 2000
Wire Wire Line
	2675 1900 2250 1900
Wire Wire Line
	2675 1800 2250 1800
Wire Wire Line
	2675 1700 2250 1700
Wire Wire Line
	2675 1600 2250 1600
Wire Wire Line
	2675 1500 2250 1500
Wire Wire Line
	2675 1400 2250 1400
Wire Wire Line
	2675 1300 2250 1300
Wire Wire Line
	2250 2600 2900 2600
Wire Wire Line
	2250 4650 2900 4650
Wire Wire Line
	2250 4550 4875 4550
Wire Wire Line
	2250 4450 4675 4450
Wire Wire Line
	2250 4350 4550 4350
Wire Wire Line
	2900 4050 2250 4050
Wire Wire Line
	2900 3950 2250 3950
Wire Wire Line
	2250 3575 2900 3575
Wire Wire Line
	2250 3475 2900 3475
Wire Wire Line
	2250 2500 2900 2500
Wire Wire Line
	2900 3175 2250 3175
Wire Wire Line
	2900 3075 2250 3075
Wire Wire Line
	2250 3275 2900 3275
Wire Wire Line
	2250 2300 2900 2300
Wire Wire Line
	2250 2200 2900 2200
Wire Wire Line
	8975 6100 9500 6100
Wire Wire Line
	9500 5800 8975 5800
Wire Wire Line
	8975 5700 9500 5700
Wire Wire Line
	8275 1675 8275 1600
Wire Notes Line
	10325 5225 10325 6525
Wire Wire Line
	9350 3325 9225 3325
Wire Wire Line
	9350 3275 9350 3325
Wire Wire Line
	9325 3525 9325 3575
Wire Wire Line
	9225 3525 9325 3525
Wire Wire Line
	9225 3425 9675 3425
Wire Wire Line
	8475 3525 8725 3525
Wire Wire Line
	8275 3325 8725 3325
Wire Wire Line
	8275 3425 8725 3425
Wire Notes Line
	8150 3925 11225 3925
Wire Wire Line
	9825 4425 9025 4425
Wire Wire Line
	9825 4625 9025 4625
Wire Wire Line
	8650 1250 8650 1575
Connection ~ 8650 1875
Wire Wire Line
	8650 1775 8650 1875
Wire Wire Line
	8650 1875 9150 1875
Wire Wire Line
	8650 1775 9150 1775
Wire Wire Line
	8275 1675 8400 1675
Wire Wire Line
	8650 1575 9150 1575
Wire Wire Line
	8800 1475 9150 1475
Wire Wire Line
	10200 1350 11000 1350
Wire Wire Line
	10200 1250 10825 1250
Text Label 2900 3475 2    60   ~ 0
D26
Text Label 2900 3575 2    60   ~ 0
D27
Text Label 10825 2300 2    60   ~ 0
D5
Text Label 10825 2400 2    60   ~ 0
D4
Text Label 10825 1050 2    60   ~ 0
D22-SCL
Text Label 10825 1150 2    60   ~ 0
D23-SDA
Text Label 10825 2600 2    60   ~ 0
D2-RX1
Text Label 10825 2500 2    60   ~ 0
D3-TX1
Text Label 10825 2200 2    60   ~ 0
D6
Text Label 10825 1750 2    60   ~ 0
D10
Text Label 10825 2100 2    60   ~ 0
D7
Text Label 10825 1950 2    60   ~ 0
D8
Text Label 10825 3150 2    60   ~ 0
D30
Text Label 10825 1850 2    60   ~ 0
D9
Text Label 10825 3050 2    60   ~ 0
D31
Wire Wire Line
	10825 1750 10200 1750
Wire Wire Line
	10825 3050 10200 3050
Wire Wire Line
	10825 1850 10200 1850
Wire Wire Line
	10825 1950 10200 1950
Wire Wire Line
	10825 3150 10200 3150
Wire Wire Line
	10200 3250 10825 3250
Wire Wire Line
	10200 3350 10825 3350
Wire Wire Line
	10200 2500 10825 2500
Wire Wire Line
	10200 2600 10825 2600
Wire Wire Line
	10825 1150 10200 1150
Wire Wire Line
	10825 1050 10200 1050
Wire Wire Line
	10825 2100 10200 2100
Wire Wire Line
	10200 2200 10825 2200
Wire Wire Line
	10200 2300 10825 2300
Wire Wire Line
	10200 2400 10825 2400
Text Label 10825 3350 2    60   ~ 0
D26
Text Label 10825 3250 2    60   ~ 0
D27
Text Label 10825 3750 2    60   ~ 0
D22-SCL
Text Label 10825 3650 2    60   ~ 0
D23-SDA
Wire Wire Line
	10825 3650 10200 3650
Wire Wire Line
	10825 3750 10200 3750
Wire Wire Line
	10825 3450 10200 3450
Wire Wire Line
	10825 3550 10200 3550
Text Label 10825 3550 2    60   ~ 0
D24
Text Label 10825 3450 2    60   ~ 0
D25
Text Notes 2950 2200 0    60   ~ 0
speed1
Text Notes 2950 2300 0    60   ~ 0
speed2
Text Notes 2950 3475 0    60   ~ 0
1_wire_PWR
Text Notes 2950 3575 0    60   ~ 0
1_wire_data
Text Notes 3000 3950 0    60   ~ 0
FTDI
Text Notes 3000 4050 0    60   ~ 0
FTDI
Text Label 9675 3425 2    60   ~ 0
D11-MOSI
Text Label 8275 3325 0    60   ~ 0
D12-MISO
Text Label 8275 3425 0    60   ~ 0
D13-SCLK
Text Label 10825 1650 2    60   ~ 0
D11-MOSI
Text Label 10825 1550 2    60   ~ 0
D12-MISO
Text Label 10825 1450 2    60   ~ 0
D13-SCLK
Wire Wire Line
	10200 1450 11050 1450
Wire Wire Line
	10825 1550 10200 1550
Wire Wire Line
	10825 1650 10200 1650
Wire Wire Line
	2250 4150 4125 4150
Wire Wire Line
	2250 4250 4325 4250
Wire Wire Line
	9150 1375 8400 1375
Wire Wire Line
	8400 1375 8400 1675
Connection ~ 8400 1675
NoConn ~ 9150 1975
$Sheet
S 9825 4050 1275 1050
U 5886192F
F0 "power_supply" 60
F1 "power_supply.sch" 60
F2 "UDC" I L 9825 4425 60 
F3 "Uac_hi" I L 9825 4625 60 
$EndSheet
$Comp
L digital-rescue:R-Device-digital-rescue R2
U 1 1 5892FF65
P 11050 1700
F 0 "R2" V 11130 1700 50  0000 C CNN
F 1 "270" V 11050 1700 50  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 10980 1700 50  0001 C CNN
F 3 "" H 11050 1700 50  0000 C CNN
	1    11050 1700
	-1   0    0    1   
$EndComp
$Comp
L digital-rescue:LED_Small-Device-digital-rescue D2
U 1 1 58930F4F
P 11050 2075
F 0 "D2" H 11000 2200 50  0000 L CNN
F 1 "Led_Small" H 10875 1975 50  0000 L CNN
F 2 "LED_THT:LED_D3.0mm_Horizontal_O1.27mm_Z2.0mm" V 11050 2075 50  0001 C CNN
F 3 "" V 11050 2075 50  0000 C CNN
	1    11050 2075
	0    -1   -1   0   
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR011
U 1 1 589311BC
P 11050 2300
F 0 "#PWR011" H 11050 2050 50  0001 C CNN
F 1 "GND" H 11050 2150 50  0000 C CNN
F 2 "" H 11050 2300 50  0000 C CNN
F 3 "" H 11050 2300 50  0000 C CNN
	1    11050 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	11050 2300 11050 2175
Wire Wire Line
	11050 1975 11050 1850
Wire Wire Line
	11050 1450 11050 1550
Wire Wire Line
	5100 3950 4325 3950
Wire Wire Line
	4325 3950 4325 4250
Wire Wire Line
	4125 4150 4125 3825
Wire Wire Line
	4125 3825 5100 3825
Wire Wire Line
	4500 2400 4500 2675
Wire Wire Line
	4500 2675 5100 2675
Wire Wire Line
	5100 4150 4550 4150
Wire Wire Line
	4550 4150 4550 4350
Wire Wire Line
	4675 4450 4675 4275
Wire Wire Line
	4675 4275 5100 4275
Wire Wire Line
	5100 4400 4875 4400
Wire Wire Line
	4875 4400 4875 4550
NoConn ~ 9500 6200
Text Notes 8550 6250 0    60   ~ 0
3.3V not connected
Text GLabel 8975 5700 0    60   Input ~ 0
USBVCC
Text Notes 9875 6200 0    60   ~ 0
+5V\nGND\nTX0\nRX0\nDTR\n3V3
Wire Wire Line
	8650 1875 8650 1950
Wire Wire Line
	8400 1675 9150 1675
Wire Wire Line
	2250 2400 4500 2400
Text Label 2900 3675 2    60   ~ 0
D28
Text Label 2900 3775 2    60   ~ 0
D29
Wire Wire Line
	9025 4525 9375 4525
Wire Wire Line
	9375 4525 9375 4725
Wire Wire Line
	9025 4725 9375 4725
Connection ~ 9375 4725
Wire Wire Line
	9375 4725 9375 4850
$Comp
L digital-rescue:GND-power-digital-rescue #PWR09
U 1 1 5B9F3BF0
P 9375 4850
F 0 "#PWR09" H 9375 4600 50  0001 C CNN
F 1 "GND" H 9380 4677 50  0000 C CNN
F 2 "" H 9375 4850 50  0001 C CNN
F 3 "" H 9375 4850 50  0001 C CNN
	1    9375 4850
	1    0    0    -1  
$EndComp
$Sheet
S 5100 2599 1225 1875
U 5804AD31
F0 "output" 60
F1 "output.sch" 60
F2 "huzzah_LDO_enable_3V3" I L 5100 4275 60 
F3 "output_RX" I L 5100 3950 60 
F4 "output_TX" O L 5100 3825 60 
F5 "output_uart_reset" I L 5100 4400 60 
F6 "huzzah_enable" I L 5100 4150 60 
F7 "SCLK" I L 5100 3000 60 
F8 "MISO" O L 5100 2900 60 
F9 "MOSI" I L 5100 2800 60 
F10 "RF_IRQ" O L 5100 2675 60 
F11 "RF_CS" I L 5100 3475 60 
F12 "BP_save_SD" I L 5100 3375 60 
F13 "SD_CS" I L 5100 3600 60 
$EndSheet
$Comp
L digital-rescue:Conn_01x06_Female-Connector-digital-rescue Mod1
U 1 1 5B98A0C0
P 5625 7175
F 0 "Mod1" H 5652 7151 50  0000 L CNN
F 1 "RTC" H 5652 7060 50  0000 L CNN
F 2 "windlog:RTC_PinHeader_1x06_P2.54mm_Vertical" H 5625 7175 50  0001 C CNN
F 3 "~" H 5625 7175 50  0001 C CNN
	1    5625 7175
	1    0    0    -1  
$EndComp
Wire Notes Line
	3375 7800 3375 6525
Wire Notes Line
	3375 6525 6975 6525
Wire Notes Line
	6975 6525 6975 6550
Text Notes 5600 6750 0    60   ~ 0
connecteur module RTC
Wire Wire Line
	5425 6975 4875 6975
Wire Wire Line
	5425 7175 4875 7175
Wire Wire Line
	5425 7275 4875 7275
Wire Wire Line
	5425 7375 4875 7375
Wire Wire Line
	4875 7475 5425 7475
Wire Wire Line
	6900 875  6900 800 
Wire Wire Line
	6900 800  6500 800 
$Comp
L digital-rescue:GND-power-digital-rescue #PWR03
U 1 1 5B9D4AED
P 6900 1850
F 0 "#PWR03" H 6900 1600 50  0001 C CNN
F 1 "GND" H 6905 1677 50  0000 C CNN
F 2 "" H 6900 1850 50  0001 C CNN
F 3 "" H 6900 1850 50  0001 C CNN
	1    6900 1850
	1    0    0    -1  
$EndComp
Text Label 6500 800  2    60   ~ 0
D24
Text Notes 6800 700  0    60   ~ 0
SD_Record\n
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR02
U 1 1 5B9E21A4
P 4875 6975
F 0 "#PWR02" H 4875 6825 50  0001 C CNN
F 1 "+5V" H 4890 7148 50  0000 C CNN
F 2 "" H 4875 6975 50  0001 C CNN
F 3 "" H 4875 6975 50  0001 C CNN
	1    4875 6975
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR01
U 1 1 5B9E2223
P 4675 7075
F 0 "#PWR01" H 4675 6825 50  0001 C CNN
F 1 "GND" H 4680 6902 50  0000 C CNN
F 2 "" H 4675 7075 50  0001 C CNN
F 3 "" H 4675 7075 50  0001 C CNN
	1    4675 7075
	1    0    0    -1  
$EndComp
Wire Wire Line
	4675 7075 5425 7075
Text Label 4875 7175 0    60   ~ 0
D23-SDA
Text Label 4875 7275 0    60   ~ 0
D22-SCL
NoConn ~ 4875 7375
NoConn ~ 4875 7475
$Comp
L digital-rescue:Conn_01x06-Connector_Generic-digital-rescue Mod2
U 1 1 586AB0B8
P 9700 5900
F 0 "Mod2" H 9700 6250 50  0000 C CNN
F 1 "USART0" V 9800 5900 50  0000 C CNN
F 2 "windlog:PinHeader_1x06_P2.54mm_Vertical" H 9700 5900 50  0001 C CNN
F 3 "" H 9700 5900 50  0000 C CNN
	1    9700 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3000 3550 3000
Wire Wire Line
	3550 3000 3550 2900
Wire Wire Line
	2250 2900 3550 2900
Wire Wire Line
	3625 2800 3625 2900
Wire Wire Line
	3625 2900 5100 2900
Wire Wire Line
	3700 2700 3700 2800
Wire Wire Line
	3700 2800 5100 2800
Wire Wire Line
	3775 3475 5100 3475
Wire Wire Line
	2250 3375 5100 3375
Wire Wire Line
	6900 1675 6900 1850
$Comp
L digital-rescue:R-Device-digital-rescue R1
U 1 1 5B9D64FB
P 6900 1025
F 0 "R1" H 6970 1071 50  0000 L CNN
F 1 "270" H 6970 980 50  0000 L CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6830 1025 50  0001 C CNN
F 3 "~" H 6900 1025 50  0001 C CNN
	1    6900 1025
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 1175 6900 1475
Wire Wire Line
	2250 2700 3700 2700
Wire Wire Line
	2250 2800 3625 2800
Text Label 8725 2825 0    60   ~ 0
D15-A6
Text Label 8725 2925 0    60   ~ 0
D14-A7
Text Label 8725 2225 0    60   ~ 0
D21-A0
Text Label 8725 2325 0    60   ~ 0
D20-A1
Text Label 8725 2425 0    60   ~ 0
D19-A2
Text Label 8725 2525 0    60   ~ 0
D18-A3
Text Label 8725 2625 0    60   ~ 0
D17-A4
Text Label 8725 2725 0    60   ~ 0
D16-A5
Wire Wire Line
	8725 2925 9150 2925
Wire Wire Line
	8725 2825 9150 2825
Wire Wire Line
	8725 2725 9150 2725
Wire Wire Line
	8725 2625 9150 2625
Wire Wire Line
	8725 2525 9150 2525
Wire Wire Line
	8725 2425 9150 2425
Wire Wire Line
	8725 2325 9150 2325
Wire Wire Line
	8725 2225 9150 2225
Wire Wire Line
	3775 3475 3775 3675
Wire Wire Line
	2250 3675 3775 3675
Wire Wire Line
	3950 3600 3950 3775
Wire Wire Line
	3950 3600 5100 3600
Wire Wire Line
	2250 3775 3950 3775
Text Label 10825 2700 2    60   ~ 0
D0-RX0
Text Label 10825 2800 2    60   ~ 0
D1-TX0
Wire Wire Line
	10200 2700 10825 2700
Wire Wire Line
	10200 2800 10825 2800
Wire Wire Line
	9500 6000 8875 6000
Wire Wire Line
	9500 5900 8875 5900
Text Label 8875 6000 0    60   ~ 0
D1-TX0
Text Label 8875 5900 0    60   ~ 0
D0-RX0
Wire Notes Line
	8150 6525 8150 500 
Text Notes 2800 1400 0    60   ~ 0
IO_micro_2
Text Notes 2775 1275 0    60   ~ 0
IO_micro_1
Text Notes 2925 3275 0    60   ~ 0
LED_SD_Record\n
$Comp
L digital-rescue:LED_Small-Device-digital-rescue D1
U 1 1 5BEECA84
P 6900 1575
F 0 "D1" H 6850 1700 50  0000 L CNN
F 1 "Led_Small" H 6725 1475 50  0000 L CNN
F 2 "LED_THT:LED_D3.0mm_Horizontal_O1.27mm_Z2.0mm" V 6900 1575 50  0001 C CNN
F 3 "" V 6900 1575 50  0000 C CNN
	1    6900 1575
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
