EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "Windlogger digital"
Date "2020-02-21"
Rev "1.2.1"
Comp "ALEEA"
Comment1 "LONGUET Gilles"
Comment2 "AGPL3"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L digital-rescue:C-Device-digital-rescue C2
U 1 1 58752FE3
P 3800 1975
F 0 "C2" H 3800 2075 40  0000 L CNN
F 1 "18p" H 3806 1890 40  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 3900 1825 30  0001 C CNN
F 3 "~" H 3800 1975 60  0000 C CNN
	1    3800 1975
	0    -1   -1   0   
$EndComp
$Comp
L digital-rescue:C-Device-digital-rescue C3
U 1 1 58752FEA
P 3800 2375
F 0 "C3" H 3800 2475 40  0000 L CNN
F 1 "18p" H 3806 2290 40  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L4.0mm_W2.5mm_P2.50mm" H 3838 2225 30  0001 C CNN
F 3 "~" H 3800 2375 60  0000 C CNN
	1    3800 2375
	0    -1   -1   0   
$EndComp
$Comp
L digital-rescue:R-Device-digital-rescue R3
U 1 1 58752FF8
P 3250 1550
F 0 "R3" V 3330 1550 40  0000 C CNN
F 1 "470k" V 3257 1551 40  0000 C CNN
F 2 "windlog:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 1550 30  0001 C CNN
F 3 "~" H 3250 1550 30  0000 C CNN
	1    3250 1550
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:C-Device-digital-rescue C4
U 1 1 58752FFF
P 3800 4750
F 0 "C4" H 3800 4850 40  0000 L CNN
F 1 "100n" H 3806 4665 40  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3900 4600 30  0001 C CNN
F 3 "~" H 3800 4750 60  0000 C CNN
	1    3800 4750
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:C-Device-digital-rescue C6
U 1 1 58753006
P 4100 4750
F 0 "C6" H 4100 4850 40  0000 L CNN
F 1 "100n" H 4106 4665 40  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 4200 4600 30  0001 C CNN
F 3 "~" H 4100 4750 60  0000 C CNN
	1    4100 4750
	1    0    0    -1  
$EndComp
Text Label 3950 2975 0    60   ~ 0
AREF
$Comp
L digital-rescue:C-Device-digital-rescue C5
U 1 1 5875301E
P 3950 3200
F 0 "C5" H 3950 3300 40  0000 L CNN
F 1 "100n" H 3956 3115 40  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 4050 3050 30  0001 C CNN
F 3 "~" H 3950 3200 60  0000 C CNN
	1    3950 3200
	1    0    0    -1  
$EndComp
Text Label 3850 1775 0    60   ~ 0
RESET
Text Notes 3575 2575 0    60   ~ 0
Main crystal
$Comp
L digital-rescue:GND-power-digital-rescue #PWR016
U 1 1 58753045
P 3800 4975
F 0 "#PWR016" H 3800 4725 50  0001 C CNN
F 1 "GND" H 3800 4825 50  0000 C CNN
F 2 "" H 3800 4975 50  0000 C CNN
F 3 "" H 3800 4975 50  0000 C CNN
	1    3800 4975
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR015
U 1 1 5875304B
P 3800 4525
F 0 "#PWR015" H 3800 4375 50  0001 C CNN
F 1 "+5V" H 3800 4665 50  0000 C CNN
F 2 "" H 3800 4525 50  0000 C CNN
F 3 "" H 3800 4525 50  0000 C CNN
	1    3800 4525
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR019
U 1 1 58753051
P 5650 5525
F 0 "#PWR019" H 5650 5275 50  0001 C CNN
F 1 "GND" H 5650 5375 50  0000 C CNN
F 2 "" H 5650 5525 50  0000 C CNN
F 3 "" H 5650 5525 50  0000 C CNN
	1    5650 5525
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR017
U 1 1 58753057
P 3950 3400
F 0 "#PWR017" H 3950 3150 50  0001 C CNN
F 1 "GND" H 3950 3250 50  0000 C CNN
F 2 "" H 3950 3400 50  0000 C CNN
F 3 "" H 3950 3400 50  0000 C CNN
	1    3950 3400
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR013
U 1 1 58753063
P 3250 1375
F 0 "#PWR013" H 3250 1225 50  0001 C CNN
F 1 "+5V" H 3250 1515 50  0000 C CNN
F 2 "" H 3250 1375 50  0000 C CNN
F 3 "" H 3250 1375 50  0000 C CNN
	1    3250 1375
	1    0    0    -1  
$EndComp
$Comp
L digital-rescue:Crystal-Device-digital-rescue X1
U 1 1 58753069
P 4125 2150
F 0 "X1" H 4125 2300 60  0000 C CNN
F 1 "16M" H 4125 2000 60  0000 C CNN
F 2 "Crystal:Crystal_HC49-4H_Vertical" H 4125 2400 60  0001 C CNN
F 3 "~" H 4125 2150 60  0000 C CNN
	1    4125 2150
	0    -1   -1   0   
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR014
U 1 1 58753070
P 3500 2175
F 0 "#PWR014" H 3500 1925 50  0001 C CNN
F 1 "GND" H 3500 2025 50  0000 C CNN
F 2 "" H 3500 2175 50  0000 C CNN
F 3 "" H 3500 2175 50  0000 C CNN
	1    3500 2175
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 5475 5650 5525
Connection ~ 3250 1775
Wire Wire Line
	2850 1775 2850 1900
Wire Wire Line
	3950 2975 3950 3050
Wire Wire Line
	3950 3400 3950 3350
Wire Wire Line
	3250 1375 3250 1400
Wire Wire Line
	3950 2375 4125 2375
Wire Wire Line
	4125 2300 4125 2375
Connection ~ 4125 2375
Wire Wire Line
	3950 1975 4125 1975
Wire Wire Line
	4125 2000 4125 1975
Connection ~ 4125 1975
Wire Wire Line
	3625 2375 3650 2375
Wire Wire Line
	3625 1975 3625 2125
Wire Wire Line
	3625 1975 3650 1975
Wire Wire Line
	3500 2175 3500 2125
Wire Wire Line
	3500 2125 3625 2125
Connection ~ 3625 2125
Wire Wire Line
	3800 4975 3800 4925
Wire Wire Line
	3800 4525 3800 4575
$Comp
L digital-rescue:+5V-power-digital-rescue #PWR018
U 1 1 587530B7
P 5650 1325
F 0 "#PWR018" H 5650 1175 50  0001 C CNN
F 1 "+5V" H 5650 1465 50  0000 C CNN
F 2 "" H 5650 1325 50  0000 C CNN
F 3 "" H 5650 1325 50  0000 C CNN
	1    5650 1325
	1    0    0    -1  
$EndComp
Text Notes 1575 2050 0    60   ~ 0
Reset button
Wire Wire Line
	3250 1775 3250 1700
Wire Wire Line
	6500 1775 6250 1775
Wire Wire Line
	6500 1875 6250 1875
Wire Wire Line
	6500 1975 6250 1975
Wire Wire Line
	6500 2075 6250 2075
Wire Wire Line
	6500 2175 6250 2175
Wire Wire Line
	6500 2275 6250 2275
Wire Wire Line
	6500 2375 6250 2375
Wire Wire Line
	6500 2475 6250 2475
Wire Wire Line
	6500 2675 6250 2675
Wire Wire Line
	6500 2775 6250 2775
Wire Wire Line
	6500 2875 6250 2875
Wire Wire Line
	6500 2975 6250 2975
Wire Wire Line
	6500 3075 6250 3075
Wire Wire Line
	6500 3175 6250 3175
Wire Wire Line
	6500 3275 6250 3275
Wire Wire Line
	6500 3375 6250 3375
Wire Wire Line
	6500 4475 6250 4475
Wire Wire Line
	6500 4575 6250 4575
Wire Wire Line
	6500 4675 6250 4675
Wire Wire Line
	6500 4775 6250 4775
Wire Wire Line
	6500 4875 6250 4875
Wire Wire Line
	6500 4975 6250 4975
Wire Wire Line
	6500 5075 6250 5075
Wire Wire Line
	6500 5175 6250 5175
Wire Wire Line
	6500 3575 6250 3575
Wire Wire Line
	6500 3675 6250 3675
Wire Wire Line
	6500 3775 6250 3775
Wire Wire Line
	6500 3875 6250 3875
Wire Wire Line
	6500 3975 6250 3975
Wire Wire Line
	6500 4075 6250 4075
Text HLabel 6500 1775 2    60   Input ~ 0
ACD0
Text HLabel 6500 1875 2    60   Input ~ 0
ACD1
Text HLabel 6500 1975 2    60   Input ~ 0
ACD2
Text HLabel 6500 2075 2    60   Input ~ 0
ACD3
Text HLabel 6500 2175 2    60   Input ~ 0
ACD4
Text HLabel 6500 2275 2    60   Input ~ 0
ACD5
Text HLabel 6500 2375 2    60   Input ~ 0
ACD6
Text HLabel 6500 2475 2    60   Input ~ 0
ACD7
Text HLabel 6500 2675 2    60   Input ~ 0
PB0
Text HLabel 6500 2775 2    60   Input ~ 0
PB1
Text HLabel 6500 2875 2    60   Input ~ 0
PB2
Text HLabel 6500 2975 2    60   Input ~ 0
PB3
Text HLabel 6500 3075 2    60   Input ~ 0
PB4
Text HLabel 6500 3175 2    60   Input ~ 0
PB5
Text HLabel 6500 3275 2    60   Input ~ 0
PB6
Text HLabel 6500 3375 2    60   Input ~ 0
PB7
Text HLabel 6500 3575 2    60   Input ~ 0
PC0
Text HLabel 6500 3675 2    60   Input ~ 0
PC1
Text HLabel 6500 3775 2    60   Input ~ 0
PC2
Text HLabel 6500 3875 2    60   Input ~ 0
PC3
Text HLabel 6500 3975 2    60   Input ~ 0
PC4
Text HLabel 6500 4075 2    60   Input ~ 0
PC5
Text HLabel 6500 4475 2    60   Input ~ 0
PD0
Text HLabel 6500 4575 2    60   Input ~ 0
PD1
Text HLabel 6500 4675 2    60   Input ~ 0
PD2
Text HLabel 6500 4775 2    60   Input ~ 0
PD3
Text HLabel 6500 4875 2    60   Input ~ 0
PD4
Text HLabel 6500 4975 2    60   Input ~ 0
PD5
Text HLabel 6500 5075 2    60   Input ~ 0
PD6
Text HLabel 6500 5175 2    60   Input ~ 0
PD7
Text HLabel 3775 1450 0    60   Input ~ 0
RESET
Wire Wire Line
	3775 1450 3775 1775
Connection ~ 3775 1775
Text HLabel 3525 2975 0    60   Input ~ 0
AREF
Connection ~ 3950 2975
Wire Wire Line
	3525 2975 3575 2975
$Comp
L digital-rescue:PWR_FLAG-power-digital-rescue #FLG01
U 1 1 587FC76C
P 3575 2950
F 0 "#FLG01" H 3575 3045 50  0001 C CNN
F 1 "PWR_FLAG" H 3575 3130 50  0000 C CNN
F 2 "" H 3575 2950 50  0000 C CNN
F 3 "" H 3575 2950 50  0000 C CNN
	1    3575 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3575 2975 3575 2950
Connection ~ 3575 2975
Wire Wire Line
	5750 1375 5750 1475
Wire Wire Line
	5650 1375 5650 1325
Connection ~ 5650 1375
Wire Wire Line
	4100 4600 4100 4575
Wire Wire Line
	4100 4575 3800 4575
Connection ~ 3800 4575
Wire Wire Line
	4100 4900 4100 4925
Wire Wire Line
	4100 4925 3800 4925
Connection ~ 3800 4925
Wire Wire Line
	3250 1775 3775 1775
Wire Wire Line
	3625 2125 3625 2375
Wire Wire Line
	2850 1775 3250 1775
Wire Wire Line
	3575 2975 3950 2975
Wire Wire Line
	5650 1375 5750 1375
Wire Wire Line
	3800 4575 3800 4600
Wire Wire Line
	3800 4925 3800 4900
Wire Wire Line
	5650 1475 5650 1375
Wire Wire Line
	3775 1775 5050 1775
Wire Wire Line
	4125 1975 5050 1975
Wire Wire Line
	5050 2175 4475 2175
Wire Wire Line
	4475 2175 4475 2375
Wire Wire Line
	4125 2375 4475 2375
Wire Wire Line
	5050 2375 4650 2375
Wire Wire Line
	4650 2375 4650 2975
Wire Wire Line
	3950 2975 4650 2975
Text HLabel 6500 4175 2    60   Input ~ 0
PC6
Text HLabel 6500 4275 2    60   Input ~ 0
PC7
Wire Wire Line
	6500 4175 6250 4175
Wire Wire Line
	6500 4275 6250 4275
$Comp
L digital-rescue:ATmega1284P-PU-MCU_Microchip_ATmega-digital-rescue U1
U 1 1 5B926A68
P 5650 3475
F 0 "U1" H 5925 1425 50  0000 C CNN
F 1 "ATmega1284P-PU" H 5025 1475 50  0000 C CNN
F 2 "windlog:DIP-40_W15.24mm" H 5650 3475 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8272-8-bit-AVR-microcontroller-ATmega164A_PA-324A_PA-644A_PA-1284_P_datasheet.pdf" H 5650 3475 50  0001 C CNN
	1    5650 3475
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 2350 2850 2300
$Comp
L digital-rescue:SW_Push-Switch-digital-rescue SW1
U 1 1 5BB5820A
P 2850 2100
F 0 "SW1" V 2896 2052 50  0000 R CNN
F 1 "PB_RESET" V 2805 2052 50  0000 R CNN
F 2 "Button_Switch_THT:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 2850 2300 50  0001 C CNN
F 3 "" H 2850 2300 50  0001 C CNN
	1    2850 2100
	0    -1   -1   0   
$EndComp
$Comp
L digital-rescue:GND-power-digital-rescue #PWR012
U 1 1 5BB60BBE
P 2850 2350
F 0 "#PWR012" H 2850 2100 50  0001 C CNN
F 1 "GND" H 2850 2200 50  0000 C CNN
F 2 "" H 2850 2350 50  0000 C CNN
F 3 "" H 2850 2350 50  0000 C CNN
	1    2850 2350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
